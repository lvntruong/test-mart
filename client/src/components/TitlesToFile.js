import React, { useState } from "react";

const TitlesToFile = () => {
    const [titles, setTitles] = useState([]);

    const showFile = async (e) => {
        e.preventDefault();
        const reader = new FileReader();
        reader.onload = async (e) => {
            const text = (e.target.result);
            setTitles(text.split('\n'));
        };
        reader.readAsText(e.target.files[0]);
    }

    return <div className="titles-to-file">
        <div className="titles-to-file__inner">
            <div className="form__template form__template__input">
                <h3>Please choose file</h3>
                <input type="file" onChange={(e) => showFile(e)} />
            </div>
            <div>
                {titles.filter(item => item.length > 0).map((title, index) => {
                    return <div className="form__template">
                        <div className="form__template__inner">
                            <h1>{title}</h1>
                        </div>
                    </div>
                })
                }
            </div>
        </div>

    </div>
};
export default TitlesToFile;
