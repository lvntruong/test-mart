import React from "react";

const Header = () => {
    return <div className="header">
        <div className="header__inner">
            <p>Titles read to file txt</p>
        </div>
    </div>
};
export default Header;