import React from "react";
import TTM_Logo from "../assets/layouts/TTM_Logo-IconOnly-ForDarkBackgrounds.svg"

const SideBar = () => {
    return <div className="sidebar">
        <div className="sidebar__inner">
            <div className="sidebar__inner--item">
                <img src={TTM_Logo} alt="" />
            </div>
            <div className="sidebar__inner--item">
                <img src={TTM_Logo} alt="" />
                <p>library</p>
            </div>
            <div className="sidebar__inner--item">
                <img src={TTM_Logo} alt="" />
                <p>builder</p>
            </div>
            <div className="sidebar__inner--item">
                <img src={TTM_Logo} alt="" />
                <p>reports</p>
            </div>
            <div className="sidebar__inner--item">
                <img src={TTM_Logo} alt="" />
                <p>variables</p>
            </div>
            <div className="sidebar__inner--item">
                <img src={TTM_Logo} alt="" />
                <p>account</p>
            </div>
        </div>
    </div>
};
export default SideBar;