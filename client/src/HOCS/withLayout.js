import React from "react";
import { Header, SideBar } from "../layouts";

const withLayout = (Component) => (props) => {
    return (
        <div className="app">
            <SideBar />
            <div>
                <Header />
                <Component />
            </div>
        </div>
    );
};

export default withLayout;
