import React from "react";
import { TitlesToFile } from "./components";
import { Header, SideBar } from "./layouts";

function App() {
  return (
    <div className="app">
      <SideBar />
      <div className="component">
        <Header />
        <TitlesToFile />
      </div>
    </div>
  );
}

export default App;
